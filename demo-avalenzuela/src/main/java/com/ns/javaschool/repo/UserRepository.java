package com.ns.javaschool.repo;

import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.Optional;
import com.ns.javaschool.model.User;

public interface UserRepository extends CrudRepository<User, Long> {


    User findByEmail(String u);
}
