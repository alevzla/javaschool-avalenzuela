package com.ns.javaschool.controller;

import com.ns.javaschool.exceptions.CustomizedNoSuchElementException;
import com.ns.javaschool.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ns.javaschool.model.User;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController extends ResponseEntityExceptionHandler {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/register", consumes = "application/json", produces = "application/json")
    public User register(@RequestBody User u) {
        return userService.addUser(u);

    }

    @PostMapping(value = "/find", consumes = "application/json", produces = "application/json")
    public User getUser(@RequestBody User u) {
        return userService.findUserByEmail(u.getEmail());
    }

    @GetMapping(value = "/", produces = "application/json")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @DeleteMapping(value = "/delete/{id}", produces = "application/json")
    public User deleteUser(@PathVariable("id") Long id) {

       User u =  userService.deleteUser(id);

       if (u == null) {
           throw new CustomizedNoSuchElementException("User not found");
       }

       return u;
    }

    @PutMapping(value = "/update", consumes = "application/json", produces = "application/json")
    public User updateUser(@RequestBody User u) {
        return userService.addUser(u);
    }
}
