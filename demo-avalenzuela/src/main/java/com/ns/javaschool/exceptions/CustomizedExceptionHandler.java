package com.ns.javaschool.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class CustomizedExceptionHandler extends ResponseEntityExceptionHandler {

    static final int USER_NOT_FOUND = 205;

    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleUserNotFound(UserNotFoundException ex, WebRequest request) {
        ErrorDetails errorDet = new ErrorDetails(USER_NOT_FOUND, ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(errorDet, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CustomizedNoSuchElementException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final ResponseEntity<ErrorDetails> handleDataNotFound(CustomizedNoSuchElementException ex, WebRequest request) {
        ErrorDetails errorDet = new ErrorDetails(USER_NOT_FOUND, ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(errorDet, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
