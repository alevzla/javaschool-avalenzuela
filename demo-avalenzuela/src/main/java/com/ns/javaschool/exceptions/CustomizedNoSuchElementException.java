package com.ns.javaschool.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.NoSuchElementException;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class CustomizedNoSuchElementException extends NoSuchElementException {

    public CustomizedNoSuchElementException(String exception) {
        super(exception);
    }
}
