package com.ns.javaschool.exceptions;

public class ErrorDetails {
    private int code;
    private String message;
    private String detail;

    public ErrorDetails() {
        super();
    }

    public ErrorDetails(int code, String message, String detail) {
        super();
        this.code = code;
        this.message = message;
        this.detail = detail;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
