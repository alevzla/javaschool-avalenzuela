package com.ns.javaschool.service;

import com.ns.javaschool.exceptions.CustomizedNoSuchElementException;
import com.ns.javaschool.exceptions.UserNotFoundException;
import com.ns.javaschool.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.ns.javaschool.model.User;

import java.util.List;
import java.util.Optional;
import org.springframework.util.StringUtils;

@Service
public class UserService {


    private UserRepository userRepo;
    private PasswordEncoder encoder;

    public UserService(UserRepository userRepo, PasswordEncoder encoder) {
        this.userRepo = userRepo;
        this.encoder = encoder;
    }

    public User addUser(User u) {
        if (u == null) {
            throw new IllegalArgumentException("Empty user received");
        }

        if (StringUtils.isEmpty(u.getName())) {
            throw new IllegalArgumentException("Name is empty");
        } else if (u.getName().length() <= 3 || u.getName().length() >= 45) {
            throw new IllegalArgumentException("Name length must be between 3 and 45 characters");
        }

        if (StringUtils.isEmpty(u.getEmail())) {
            throw new IllegalArgumentException("Email is empty");
        } else if (!u.getEmail().matches("[a-zA-Z0-9_.]+@[a-zA-Z0-9]+.[a-zA-Z]{2,3}[.]{0,1}[a-zA-Z]+")) {
            throw new IllegalArgumentException("Invalid email address");
        }

        if (StringUtils.isEmpty(u.getPassword())) {
            throw new IllegalArgumentException("Password is empty");
        } else {
            u.setPassword(encoder.encode(u.getPassword()));
        }

        return userRepo.save(u);
    }

    public List<User> getAllUsers() {
        return (List<User>) userRepo.findAll();
    }

    public User findUserByEmail(String email) {
        if (email == null) {
            throw new IllegalArgumentException("Email is empty");
        }
        return userRepo.findByEmail(email);
    }

    public User deleteUser(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Id is empty");
        }

        Optional<User> u = userRepo.findById(id);
        if (u == null) {
            throw new IllegalArgumentException("User not found");
        }

        userRepo.delete(u.get());
        return u.get();
    }


}
