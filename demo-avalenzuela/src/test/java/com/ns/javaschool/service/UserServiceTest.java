package com.ns.javaschool.service;

import com.ns.javaschool.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserServiceTest {
    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder encoder;

    @Test
    public void addUser(){
        User user = new User("Rufina Varela", "rufi_gogo@hotmail.com", "noTS4f3paz");
        User savedUser = userService.addUser(user);
        assertThat(savedUser).isNotNull();
        assertThat(savedUser.getId()).isNotNull();
        assertThat(savedUser.getName()).isNotNull();
        assertThat(savedUser.getEmail()).isNotNull();
        assertThat(savedUser.getPassword()).isNotNull();
        assertThat(user.getEmail()).isEqualTo(savedUser.getEmail());
    }


    @Test
    public void testEncryptedAndMatchPassword() {
        String password = "ulTr4$3crT";
        String encryptedPass = encoder.encode(password);
        assertThat(encoder.matches(password, encryptedPass)).isTrue();

    }

}
