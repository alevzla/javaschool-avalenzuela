package com.ns.javaschool.controller;

import com.ns.javaschool.model.User;
import com.ns.javaschool.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {

    @Autowired
    private TestRestTemplate testTemp;

    @Autowired
    private UserService userService;


    @Test
    public void register() {
        User userOne = new User("Luis Copito Valenzuela", "chocobel@gmail.com", "copito123$");

        ResponseEntity<User> userResponseEntity = testTemp.postForEntity("/api/v1/users/register", userOne, User.class);
        User u = userResponseEntity.getBody();

        assertThat(u).isNotNull();
        assertThat(u.getId()).isNotNull();
        assertThat(u.getName()).isNotNull();
        assertThat(u.getEmail()).isNotNull();
        assertThat(u.getEmail()).isEqualTo(userOne.getEmail());
    }

    @Test
    public void getUser() {
        Map<String, String> user = new HashMap<>();
        user.put("email", "mariatest@gmail.com");
        ResponseEntity<User> userResponseEntity = testTemp.postForEntity("/api/v1/users/find", user, User.class);
        assertThat(userResponseEntity.getBody().getEmail()).isEqualTo(user.get("email"));
    }


}